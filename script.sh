#!/bin/bash

USER_TIME=7;
USER_TYPE="days";

UPTIME_TIME=$(uptime | awk '{print $3}')
UPTIME_TYPE=$(uptime | awk '{print $4}')

function main () {
        echo $UPTIME_TYPE > /tmp/.uptime_type;
        sed -i 's/,//g' /tmp/.uptime_type;
        UPTIME_TYPE_=$(cat /tmp/.uptime_type)

	if [ $UPTIME_TIME > $USER_TIME ] && [ $UPTIME_TYPE_ = $USER_TYPE ];then
                /sbin/shutdown -r;
        fi
}

main
